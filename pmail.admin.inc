<?php

/**
 * @file
 * Administration pages for personalized e-mails module.
 */

/**
 * Menu callback; generates the settings form.
 */
function pmail_settings() {
  $form['pmail_pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('Display name'),
    '#default_value' => variable_get('pmail_pattern', '[user]'),
    '#description' => t('Type the pattern to construct display names from. Use the syntax [token] to insert a replacement pattern.'),
  );

  $form['token_help'] = array(
    '#title' => t('Replacement patterns'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['token_help']['help'] = array(
    '#value' => theme('token_help', 'user'),
  );

  $form['pmail_static_mappings'] = array(
    '#type' => 'textarea',
    '#title' => t('Static display name mappings'),
    '#default_value' => pmail_static_map(),
    '#description' => t('Type in special static replacements in the form <code>mail@example.com, Display name</code>. Separate multiple entries with newlines. The default entry maps the site e-mail address to the site name (!example). Static mappings have precedence over user-specific replacement patterns.', array('!example' => '<em>'. check_plain(variable_get('site_name', t('Drupal'))) .' &lt;'. check_plain(variable_get('site_mail', ini_get('sendmail_from'))) .'&gt;</em>')),
  );

  return system_settings_form($form);
}

