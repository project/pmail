
-- SUMMARY --

This module allows you to personalize any e-mails sent from your site by
enhancing the From and Reply-To email addresses with display names, eg.
"Real Name <user@example.com>".

This is done in a fully configurable way by relying on the token module to 
provide on-the-fly substitutions, and contains a fall back on the user's login
name if the token replacement didn't yield a result (for example because a user
didn't fill in the name on its profile).

For a full description visit the project page:
  http://drupal.org/project/pmail
Bug reports, feature suggestions and latest developments:
  http://drupal.org/project/issues/pmail


-- DEPENDENCIES --

* Token
  http://drupal.org/project/token


-- INSTALLATION --

1. Install as usual, see http://drupal.org/node/70151 for further information.

2. Copy token_user.inc from the patches subdirectory to the token installation,
   thereby overwriting the existing file.

3. Go to User management >> Personalized e-mail settings and configure the
   replacement pattern to create display names from.


-- CONTACT --

Author:
Stefan M. Kudwien (smk-ka) - http://drupal.org/user/48898

